import tensorflow as tf
import random
import time
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

def next_batch(num, data1, data2, labels):
	
	idx = np.arange(0 , len(data))
	seed = random.random() * 1000
	random.seed(seed)
	random.shuffle(idx)
	idx = idx[:num]
	data1_shuffle = [data1[i] for i in idx]
	data2_shuffle = [data2[i] for i in idx]
	labels_shuffle = [labels[i] for i in idx]

	return np.asarray(data1_shuffle), np.asarray(data2_shuffle), np.asarray(labels_shuffle)
	
def conv2d(x, W, b, strides=1):
    # Conv2D wrapper, with bias and relu activation
    x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='SAME')
    x = tf.nn.bias_add(x, b)
    return tf.nn.relu(x) 

def maxpool2d(x, k=2):
    return tf.nn.max_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1],padding='SAME')
	
	
def conv_net(x, weights, biases):  

    # here we call the conv2d function we had defined above and pass the input image x, weights wc1 and bias bc1.
    conv1 = conv2d(x, weights['wc1'], biases['bc1'])
    # Max Pooling (down-sampling), this chooses the max value from a 2*2 matrix window and outputs a 14*14 matrix.
    conv1 = maxpool2d(conv1, k=2)

    # Convolution Layer
    # here we call the conv2d function we had defined above and pass the input image x, weights wc2 and bias bc2.
    conv2 = conv2d(conv1, weights['wc2'], biases['bc2'])
    # Max Pooling (down-sampling), this chooses the max value from a 2*2 matrix window and outputs a 7*7 matrix.
    conv2 = maxpool2d(conv2, k=2)

    conv3 = conv2d(conv2, weights['wc3'], biases['bc3'])
    # Max Pooling (down-sampling), this chooses the max value from a 2*2 matrix window and outputs a 4*4.
    conv3 = maxpool2d(conv3, k=2)


    # Fully connected layer
    # Reshape conv2 output to fit fully connected layer input
    fc1 = tf.reshape(conv3, [-1, weights['wd1'].get_shape().as_list()[0]])
    fc1 = tf.add(tf.matmul(fc1, weights['wd1']), biases['bd1'])
    fc1 = tf.nn.relu(fc1)
    # Output, class prediction
    # finally we multiply the fully connected layer with the weights and add a bias term. 
    out = tf.add(tf.matmul(fc1, weights['out']), biases['out'])
    return out
	
######################################################
	
learning_rate = 0.001
batch_size = 100

with tf.name_scope("variable"):
	weights1 = {
		'wc1': tf.get_variable('W0', shape=(3,3,1,32), initializer=tf.contrib.layers.xavier_initializer()), 
		'wc2': tf.get_variable('W1', shape=(3,3,32,64), initializer=tf.contrib.layers.xavier_initializer()), 
		'wc3': tf.get_variable('W2', shape=(3,3,64,128), initializer=tf.contrib.layers.xavier_initializer()), 
		'wd1': tf.get_variable('W3', shape=(4*4*128,128), initializer=tf.contrib.layers.xavier_initializer()), 
		'out': tf.get_variable('W6', shape=(128,100), initializer=tf.contrib.layers.xavier_initializer()), 
	}
	biases1 = {
		'bc1': tf.get_variable('B0', shape=(32), initializer=tf.contrib.layers.xavier_initializer()),
		'bc2': tf.get_variable('B1', shape=(64), initializer=tf.contrib.layers.xavier_initializer()),
		'bc3': tf.get_variable('B2', shape=(128), initializer=tf.contrib.layers.xavier_initializer()),
		'bd1': tf.get_variable('B3', shape=(128), initializer=tf.contrib.layers.xavier_initializer()),
		'out': tf.get_variable('B4', shape=(100), initializer=tf.contrib.layers.xavier_initializer()),
	}
	
	weights2 = weights1
	biases2 = biases1
	

with tf.name_scope("photo"):

	x1 = tf.placeholder(tf.float32, [None, 28,28,1])
	x2 = tf.placeholder(tf.float32, [None, 28,28,1])
	
	
with tf.name_scope("combine"):

	pred1 = conv_net(x1, weights1, biases1)
	pred2 = conv_net(x2, weights2, biases2)
	print(pred1)
	
	
	# fonction pour comparer les 100 sortie
	y_ = tf.placeholder(tf.float32, [None, 1])
	cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y_, logits=y))
	train_step = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cross_entropy)
	
with tf.name_scope("accuracy"):

	correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y_, 1))
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
	
with tf.name_scope("summary"):
	acc = tf.summary.scalar("accuracy", accuracy)
	cross = tf.summary.scalar("entropy", cross_entropy)
		
# Initializing the variables
init = tf.global_variables_initializer()

# tensorboard
ticks = time.time()
writer1 = tf.summary.FileWriter("tensorboard/train/"+str(ticks))
writer2 = tf.summary.FileWriter("tensorboard/test/"+str(ticks))
writer1.add_graph(sess.graph)

with tf.Session() as sess:
	sess.run(init) 
	
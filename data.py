import cv2
import numpy as np
import sys
import os
import random

def copy_img(path):
	for x in os.walk(path):
		#if directory not empty
		nameDir = x[0].split("\\")[len(x[0].split("\\"))-1]
		dir = x[0].split("\\")[len(x[0].split("\\"))-2]
		if x[2] != []:
			#for each picture in directory trouve le model
			for i in x[2]:
				#if i est le model
				if i.split(".")[0] == nameDir:
					model = i
					imgModel = cv2.imread(x[0] + "\\" + i)
			#add chaque image
			for i in x[2]:
				if i != model:
					img = cv2.imread(x[0] + "\\"+i)
					vis = np.concatenate((imgModel, img), axis=1)
					path = "C:/Users/Maxime/Documents/photo/" + dir
					a = cv2.imwrite(os.path.join(path , i), vis)

	
def setDataset(path):
	filenames = []
	labels = []
	for x in os.walk(path):
		#if directory not empty
		nameDir = x[0].split("\\")[len(x[0].split("\\"))-1]
		if x[2] != []:
			#add chaque image
			for i in x[2]:
				filenames.append((x[0] +"/" +  i))
				if nameDir == "Valid" :
					temp = [1,0]
					labels.append(temp)
				else : 
					temp = [0,1]
					labels.append(temp)
					
	return filenames,labels
					
def transform(filenames, label):
	feature = []
	labels = []
	
	for lab in label:
		lab = np.array(lab, dtype=np.float32)
		labels.append(lab)
	for file in filenames:
		img = cv2.imread(file)
		img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		img = cv2.resize(img, (28,49))
		img = np.reshape(img, (1372))
		# normalize
		img = (img)/(255)
		feature.append(img)
		
	labels = np.array(labels)
	feature = np.array(feature)
	return feature, labels
	
	
	
# copy_img(sys.argv[1])

filenames, labels = setDataset(sys.argv[1])

features, labels = transform(filenames, labels)
# shuffle list
print(features)

seed = random.random() * 100
random.seed(seed)
random.shuffle(features)
random.seed(seed)
random.shuffle(labels)



#save
np.save('feature.npy', features)
np.save('labels.npy', labels)

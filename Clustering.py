import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D, UpSampling2D
import matplotlib.pyplot as plt
from keras import backend as K
import numpy as np
from tensorflow.contrib.factorization.python.ops import clustering_ops
import tensorflow as tf

# transforme donnée
########################################################
models = np.load('models.npy')

x_train = models[:round((len(models)*(80/100)))]
x_test = models[round((len(models)*(80/100))):]

########################################################

model = Sequential()

#1st convolution layer
model.add(Conv2D(16, (3, 3) #16 is number of filters and (3, 3) is the size of the filter.
    , padding='same', input_shape=(200,200,1)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2), padding='same'))

#2nd convolution layer
model.add(Conv2D(4,(3, 3), padding='same')) # apply 2 filters sized of (3x3)
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2), padding='same'))

#3nd convolution layer
model.add(Conv2D(2,(3, 3), padding='same')) # apply 2 filters sized of (3x3)
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2), padding='same'))

#-------------------------

#3rd convolution layer
model.add(Conv2D(2,(3, 3), padding='same')) # apply 2 filters sized of (3x3)
model.add(Activation('relu'))
model.add(UpSampling2D((2, 2)))

#3rd convolution layer
model.add(Conv2D(4,(3, 3), padding='same')) # apply 2 filters sized of (3x3)
model.add(Activation('relu'))
model.add(UpSampling2D((2, 2)))

#4rd convolution layer
model.add(Conv2D(16,(3, 3), padding='same'))
model.add(Activation('relu'))
model.add(UpSampling2D((2, 2)))

#-------------------------

model.add(Conv2D(1,(3, 3), padding='same'))
model.add(Activation('sigmoid'))

model.summary()

#########################################


model.compile(optimizer='adadelta', loss='binary_crossentropy')

model.fit(x_train, x_train
    , epochs=3
    , validation_data=(x_test, x_test)
)

restored_imgs = model.predict(x_test)

for i in range(5):
    plt.imshow(x_test[i].reshape(200, 200))
    plt.gray()
    plt.show()
    
    plt.imshow(restored_imgs[i].reshape(200, 200))
    plt.gray()
    plt.show()
    
    print("----------------------------")

	
########################################################
#Clustering
########################################################
	
# layer[7] is activation_3 (Activation), it is compressed representation
get_3rd_layer_output = K.function([model.layers[0].input], [model.layers[10].output])
compressed = get_3rd_layer_output([x_test])[0]

# layer[7] is size of (None, 7, 7, 2). this means 2 different 7x7 sized matrixes. We will flatten these matrixes.
compressed = compressed.reshape(538,25*25*2)
	
unsupervised_model = tf.contrib.learn.KMeansClustering(
    10 ##num of clusters
    , distance_metric = clustering_ops.SQUARED_EUCLIDEAN_DISTANCE
    , initial_clusters=tf.contrib.learn.KMeansClustering.RANDOM_INIT
)
	
def train_input_fn():
    data = tf.constant(compressed, tf.float32)
    return (data, None)
	
unsupervised_model.fit(input_fn=train_input_fn, steps=1000)

clusters = unsupervised_model.predict(input_fn=train_input_fn)

index = 0
for i in clusters:
    current_cluster = i['cluster_idx']
    features = x_test[index]
    
    if index < 5 and current_cluster == 5:
        plt.imshow(x_test[index].reshape(200, 200))
        plt.gray()
        plt.show()
    index = index + 1

	
	
	
	
	

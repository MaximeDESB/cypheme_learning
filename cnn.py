
from matplotlib import pyplot as plt
import tensorflow as tf
import csv
import sys
import numpy as np
import random
import time
# Just disables the warning, doesn't enable AVX/FMA
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


BATCH_SIZE = 100

def next_batch(num, data, labels):
	
	idx = np.arange(0 , len(data))
	seed = random.random() * 1000
	random.seed(seed)
	random.shuffle(idx)
	idx = idx[:num]
	data_shuffle = [data[i] for i in idx]
	labels_shuffle = [labels[i] for i in idx]

	return np.asarray(data_shuffle), np.asarray(labels_shuffle)

# import la base de donnée
features = np.load('feature.npy')
labels = np.load('labels.npy')

train_features = features[:round((len(features)*(80/100)))]
train_labels = labels[:round((len(labels)*(80/100)))]

test_features = features[round((len(features)*(80/100))):]
test_labels = labels[round((len(labels)*(80/100))):]


with tf.name_scope("train"):

	x = tf.placeholder(tf.float32, [None,1372], name="photo")

	W1 = tf.Variable(tf.truncated_normal([1372, 100]))
	W2 = tf.Variable(tf.truncated_normal([100, 10]))
	W3 = tf.Variable(tf.truncated_normal([10, 2]))
	b1 = tf.Variable(tf.truncated_normal([100]))
	b2 = tf.Variable(tf.truncated_normal([10]))
	b3 = tf.Variable(tf.truncated_normal([2]))
	
	tf.summary.histogram("weight1", W1)
	tf.summary.histogram("weight2", W2)
	tf.summary.histogram("weight3", W3)
	tf.summary.histogram("bias1", b1)
	tf.summary.histogram("bias2", b2)
	tf.summary.histogram("bias3", b3)

	input =tf.nn.relu(tf.matmul(x, W1) + b1)
	hidden1 =tf.nn.relu(tf.matmul(input, W2) + b2)
	output =tf.nn.relu(tf.matmul(hidden1, W3) + b3)
	
	tf.summary.histogram("hidden1", hidden1)
	tf.summary.histogram("output", output)
	
	
	y = tf.nn.softmax(output)
	
	y_ = tf.placeholder(tf.float32, [None,2])

	# cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(tf.clip_by_value(y,1e-10,1.0)), reduction_indices=[1]))
	# cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))
	cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y_, logits=y))
	tf.summary.scalar("entropy", cross_entropy)
	train_step = tf.train.AdamOptimizer().minimize(cross_entropy)
	
	correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))	
	summ = tf.summary.scalar("accuracy", accuracy)

	
sess = tf.Session()
ticks = time.time()
writer1 = tf.summary.FileWriter("tensorboard/train/"+str(ticks),sess.graph)
writer2 = tf.summary.FileWriter("tensorboard/test/"+str(ticks),sess.graph)
writer1.add_graph(sess.graph)

init = tf.global_variables_initializer()
sess.run(init)


merged = tf.summary.merge_all()

for i in range(10000):
	batch_xs, batch_ys = next_batch(BATCH_SIZE, train_features, train_labels)
	
	sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
	if i % 100 == 0:
		s = sess.run(merged, feed_dict={x: batch_xs, y_: batch_ys})
		writer1.add_summary(s,i)
		
		a= sess.run(summ, feed_dict={x: test_features, y_: test_labels})
		writer2.add_summary(a,i)

print(sess.run(accuracy, feed_dict={x: test_features, y_: test_labels}))
			




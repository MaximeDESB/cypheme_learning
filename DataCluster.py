import cv2
import numpy as np
import sys
import os
import random


def input_img(path):
	list = []
	for x in os.walk(path):
		#if directory not empty
		nameDir = x[0].split("\\")[len(x[0].split("\\"))-1]
		dir = x[0].split("\\")[len(x[0].split("\\"))-2]
		if x[2] != []:
			#for each picture in directory trouve le model
			for i in x[2]:
				list.append(transform(x[0] + "\\"+i))
	
	return np.array(list)
			
			
def transform(file):

	img = cv2.imread(file)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	img = cv2.resize(img, (200,200))
	img = np.reshape(img, (200,200,1))
	# normalize
	img = (img)/(255)
	return img
	
	
models = input_img(sys.argv[1])
print(models[0])
print('len', len(models))

np.save('models.npy', models)